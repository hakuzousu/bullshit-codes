public class LogPrint {
    private static final Logger logger = LoggerFactory.getLogger(LogPrint.class);

    /**
     * 只打印异常信息，不打印堆栈信息，出问题找不到错误
     */
    public static void main(String[] args) {
        try {
            //业务逻辑
        } catch (Exception e){
            logger.error("业务异常：" + e.getMessage());
        }
    }
}