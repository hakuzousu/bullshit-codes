"""
这是以前碰到的一个神奇的问题抽象出来的demo，现在也不是很清楚为什么
下面给出来的代码是可以跑的
然而一旦把a=1换为注释的那段代码就会报SyntaxError
仅仅只是加了一个type hint而已啊
离谱
"""

a = None

def global_init():
    global a
    a = 1
    #a:int = 1

global_init()
print(a)